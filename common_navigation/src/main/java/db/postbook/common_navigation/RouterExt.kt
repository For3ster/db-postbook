package db.postbook.common_navigation

import androidx.fragment.app.Fragment
import com.github.terrakok.cicerone.Router
import db.postbook.common_navigation.di.RouterHolder

inline val Fragment.router: Router
    get() = (requireContext().applicationContext as RouterHolder).router
