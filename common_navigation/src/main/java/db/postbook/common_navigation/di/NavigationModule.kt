package db.postbook.common_navigation.di

import com.github.terrakok.cicerone.Cicerone
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object NavigationModule {

    private val cicerone = Cicerone.create()

    @Singleton
    @Provides
    fun provideMainRouter() = cicerone.router

    @Singleton
    @Provides
    fun provideMainNavHolder() = cicerone.getNavigatorHolder()
}
