package db.postbook.domain_posts.dto

data class Post(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String,
)
