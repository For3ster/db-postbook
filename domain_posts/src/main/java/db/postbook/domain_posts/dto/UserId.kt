package db.postbook.domain_posts.dto

@JvmInline
value class UserId(val id: Int)
