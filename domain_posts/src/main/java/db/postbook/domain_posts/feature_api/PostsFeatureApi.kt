package db.postbook.domain_posts.feature_api

import com.github.terrakok.cicerone.Screen
import db.postbook.domain_posts.dto.UserId

interface PostsFeatureApi {

    fun getScreen(userId: UserId): Screen
}
