package db.postbook.domain_posts

import db.postbook.domain_posts.dto.Comment
import db.postbook.domain_posts.dto.Post
import retrofit2.http.GET
import retrofit2.http.Path

private const val POST_ID_PATH = "postId"

interface PostsApi {

    @GET("posts")
    suspend fun getPosts(): List<Post>

    @GET("posts/{$POST_ID_PATH}")
    suspend fun getPost(
        @Path(POST_ID_PATH) postId: Int
    ): Post

    @GET("posts/{$POST_ID_PATH}/comments")
    suspend fun getComments(
        @Path(POST_ID_PATH) postId: Int
    ): List<Comment>
}
