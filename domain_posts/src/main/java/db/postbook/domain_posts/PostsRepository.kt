package db.postbook.domain_posts

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringSetPreferencesKey
import db.postbook.domain_posts.dto.Post
import db.postbook.domain_posts.dto.UserId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostsRepository @Inject constructor(
    private val postsApi: PostsApi,
    private val dataStore: DataStore<Preferences>
) {

    private lateinit var favPostsKey: Preferences.Key<Set<String>>

    suspend fun getUserPosts(userId: UserId): List<Post> {
        initKey(userId)
        return postsApi.getPosts().filter { it.userId == userId.id }
    }

    fun observeFavoritePostIds(): Flow<Set<String>> =
        dataStore.data.map { prefs ->
            prefs[favPostsKey].orEmpty()
        }

    suspend fun addPostToFavorites(postId: Int) {
        dataStore.edit { prefs ->
            prefs[favPostsKey] = prefs[favPostsKey].orEmpty() + postId.toString()
        }
    }

    suspend fun removePostFromFavorites(postId: Int) {
        dataStore.edit { prefs ->
            prefs[favPostsKey] = prefs[favPostsKey].orEmpty() - postId.toString()
        }
    }

    private fun initKey(userId: UserId) {
        if (!this::favPostsKey.isInitialized) {
            favPostsKey = stringSetPreferencesKey("PREFS_FAVOURITE_POSTS_${userId.id}")
        }
    }
}
