package db.postbook.domain_login

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import db.postbook.domain_posts.dto.UserId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginRepository @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {

    private val userIdKey = intPreferencesKey("PREFS_USER_ID")

    suspend fun setUserId(id: UserId) {
        dataStore.edit { prefs ->
            prefs[userIdKey] = id.id
        }
    }

    fun observeUserId(): Flow<UserId?> =
        dataStore.data.map { prefs ->
            prefs[userIdKey]?.let(::UserId)
        }
}
