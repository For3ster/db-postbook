package db.postbook.domain_login.feature_api

import com.github.terrakok.cicerone.Screen

interface LoginFeatureApi {

    fun getScreen(): Screen
}
