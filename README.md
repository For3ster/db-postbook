# Postbook - Challenge

The decisions made in this project were intended to provide an example of the implementation of a potentially scalable project.
For smaller applications, simpler solutions can be used, such as abandoning multimodularity, using Hilt instead of Dagger, etc.


### Tech info

  - [Kotlin](https://kotlinlang.org)
  - Multi-module project
  - MVI Architecture
  - Min SDK 24

### Libraries Used

* [Elmslie](https://github.com/vivid-money/elmslie) - architecture core library. MVI-like, inspired by ELM.
* [Dagger](https://github.com/google/dagger) for dependency injection
* [OkHttp](https://github.com/square/okhttp) and [Retrofit](https://github.com/square/retrofit) as a Http client
* [Moshi](https://github.com/square/moshi) for data parsing
* [DataStore](https://developer.android.com/topic/libraries/architecture/datastore) as a data storage
* [Cicerone](https://github.com/terrakok/Cicerone) - lightweight navigation library which is convenient for multi-module projects
* [ViewBinding Delegate](https://github.com/androidbroadcast/ViewBindingPropertyDelegate) to make work with Android View Binding simpler
* [JUnit 5](https://github.com/junit-team/junit5) - testing framework
* [Kotest](https://kotest.io/) - testing framework
* [Mockito](https://site.mockito.org/) and [Mockito Kotlin](https://github.com/mockito/mockito-kotlin) as a mocking framework for unit tests
