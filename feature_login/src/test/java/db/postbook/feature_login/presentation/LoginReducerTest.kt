package db.postbook.feature_login.presentation

import db.postbook.domain_posts.dto.UserId
import db.postbook.feature_login.presentation.LoginCommand.ObserveUserId
import db.postbook.feature_login.presentation.LoginCommand.SetUserId
import db.postbook.feature_login.presentation.LoginEffect.OpenPosts
import db.postbook.feature_login.presentation.LoginEvent.Internal
import db.postbook.feature_login.presentation.LoginEvent.Ui
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe

internal class LoginReducerTest : BehaviorSpec({
    val reducer = LoginReducer

    Given("Initial State") {
        When("Ui.System.Init") {
            val (state, effects, commands) = reducer.reduce(Ui.System.Init, STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") {
                commands.shouldContainExactly(ObserveUserId)
            }
        }

        When("Internal.ObserveUserIdSuccess") {
            val (state, effects, commands) =
                reducer.reduce(Internal.ObserveUserIdSuccess(USER_ID), STATE)

            Then("check state") { state.userId shouldBe USER_ID }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Ui.Click.Login") {
            val (state, effects, commands) = reducer.reduce(Ui.Click.Login(USER_ID.id), STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldContainExactly(SetUserId(USER_ID)) }
        }

        When("Internal.SetUserIdSuccess") {
            val (state, effects, commands) =
                reducer.reduce(Internal.SetUserIdSuccess(USER_ID), STATE)

            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldContainExactly(OpenPosts(USER_ID)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }

    Given("Empty UserId") {
        When("Ui.Click.Login") {
            val (state, effects, commands) = reducer.reduce(Ui.Click.Login(null), STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }
}) {

    private companion object {

        val USER_ID = UserId(1)
        val STATE = LoginState(userId = USER_ID)
    }
}
