package db.postbook.feature_login.di

import dagger.Binds
import dagger.Module
import db.postbook.domain_login.feature_api.LoginFeatureApi
import db.postbook.feature_login.feature_api.LoginFeatureApiImpl
import db.postbook.feature_login.presentation.LoginStoreFactory
import db.postbook.feature_login.presentation.LoginStoreFactoryImpl

@Module
abstract class LoginModule {

    @Binds
    internal abstract fun LoginFeatureApiImpl.bindLoginFeatureApi(): LoginFeatureApi

    @Binds
    internal abstract fun LoginStoreFactoryImpl.bindLoginStoreFactory(): LoginStoreFactory
}
