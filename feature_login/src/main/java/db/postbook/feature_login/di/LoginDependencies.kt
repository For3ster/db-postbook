package db.postbook.feature_login.di

import db.postbook.domain_posts.feature_api.PostsFeatureApi
import db.postbook.feature_login.presentation.LoginStoreFactory

interface LoginDependencies {

    val loginStoreFactory: LoginStoreFactory
    val postsFeatureApi: PostsFeatureApi
}
