package db.postbook.feature_login.presentation

import db.postbook.common_mvi.actorFlow
import db.postbook.domain_login.LoginRepository
import db.postbook.feature_login.presentation.LoginCommand.ObserveUserId
import db.postbook.feature_login.presentation.LoginCommand.SetUserId
import db.postbook.feature_login.presentation.LoginEvent.Internal
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterNotNull
import vivid.money.elmslie.coroutines.Actor
import javax.inject.Inject

internal class LoginActor @Inject constructor(
    private val repository: LoginRepository,
) : Actor<LoginCommand, Internal> {

    override fun execute(command: LoginCommand): Flow<Internal> =
        when (command) {
            is ObserveUserId ->
                repository.observeUserId()
                    .filterNotNull()
                    .mapEvents(Internal::ObserveUserIdSuccess)

            is SetUserId -> actorFlow {
                repository.setUserId(command.userId)
            }.mapEvents({ Internal.SetUserIdSuccess(command.userId) })
        }
}
