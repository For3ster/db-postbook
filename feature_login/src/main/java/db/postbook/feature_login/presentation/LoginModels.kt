package db.postbook.feature_login.presentation

import db.postbook.domain_posts.dto.UserId
import vivid.money.elmslie.core.store.Store

typealias LoginStore = Store<LoginEvent, LoginEffect, LoginState>

data class LoginState(
    val userId: UserId? = null,
)

sealed interface LoginEvent {

    sealed interface Ui : LoginEvent {
        object System {
            object Init : Ui
        }

        object Click {
            data class Login(val userId: Int?) : Ui
        }
    }

    sealed interface Internal : LoginEvent {
        data class ObserveUserIdSuccess(val userId: UserId?) : Internal

        data class SetUserIdSuccess(val userId: UserId) : Internal
    }
}

sealed interface LoginEffect {
    data class OpenPosts(val userId: UserId) : LoginEffect
}

sealed interface LoginCommand {
    object ObserveUserId : LoginCommand

    data class SetUserId(val userId: UserId) : LoginCommand
}
