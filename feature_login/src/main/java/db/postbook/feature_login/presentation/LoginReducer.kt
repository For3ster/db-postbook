package db.postbook.feature_login.presentation

import db.postbook.domain_posts.dto.UserId
import db.postbook.feature_login.presentation.LoginEvent.Internal
import db.postbook.feature_login.presentation.LoginEvent.Ui
import vivid.money.elmslie.core.store.dsl_reducer.ScreenDslReducer
import db.postbook.feature_login.presentation.LoginCommand as Command
import db.postbook.feature_login.presentation.LoginEffect as Effect
import db.postbook.feature_login.presentation.LoginEvent as Event
import db.postbook.feature_login.presentation.LoginState as State

internal object LoginReducer : ScreenDslReducer<Event, Ui, Internal,
        State, Effect, Command>(Ui::class, Internal::class) {

    override fun Result.ui(event: Ui) = when (event) {
        is Ui.System.Init -> commands { +Command.ObserveUserId }
        is Ui.Click.Login -> commands {
            +event.userId?.let { Command.SetUserId(UserId(it)) }
        }
    }

    override fun Result.internal(event: Internal) = when (event) {
        is Internal.ObserveUserIdSuccess -> state { copy(userId = event.userId) }
        is Internal.SetUserIdSuccess -> effects { +Effect.OpenPosts(event.userId) }
    }
}
