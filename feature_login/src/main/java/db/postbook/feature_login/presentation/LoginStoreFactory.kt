package db.postbook.feature_login.presentation

import vivid.money.elmslie.coroutines.ElmStoreCompat
import javax.inject.Inject

interface LoginStoreFactory {

    fun create(): LoginStore
}

internal class LoginStoreFactoryImpl @Inject constructor(
    private val actor: LoginActor
) : LoginStoreFactory {

    override fun create(): LoginStore = ElmStoreCompat(
        initialState = LoginState(),
        reducer = LoginReducer,
        actor = actor
    )
}
