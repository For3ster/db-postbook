package db.postbook.feature_login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.textfield.TextInputEditText
import db.postbook.common_di.getComponent
import db.postbook.common_mvi.base.BaseFragment
import db.postbook.common_mvi.setOnBackPressedCallback
import db.postbook.common_navigation.router
import db.postbook.feature_login.databinding.FragmentLoginBinding
import db.postbook.feature_login.di.LoginDependencies
import db.postbook.feature_login.presentation.LoginEvent.Ui
import db.postbook.feature_login.presentation.LoginState
import vivid.money.elmslie.storepersisting.retainInActivityStoreHolder
import db.postbook.feature_login.presentation.LoginEffect as Effect
import db.postbook.feature_login.presentation.LoginEvent as Event
import db.postbook.feature_login.presentation.LoginState as State

internal class LoginFragment : BaseFragment<Event, Effect, State>(R.layout.fragment_login) {

    private val component by lazy { getComponent<LoginDependencies>() }

    override val initEvent: Event = Ui.System.Init

    override val storeHolder by retainInActivityStoreHolder(
        storeProvider = { component.loginStoreFactory.create() }
    )

    private val binding by viewBinding(FragmentLoginBinding::bind)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginButton.setOnClickListener {
            binding.userIdInputLayout.clearFocus()
            store.accept(Ui.Click.Login(binding.userIdEditText.getIntInput()))
        }

        setOnBackPressedCallback { router.exit() }
    }

    override fun render(state: LoginState) {
        state.userId?.let { binding.userIdEditText.setText(it.id.toString()) }
    }

    override fun handleEffect(effect: Effect) = when (effect) {
        is Effect.OpenPosts -> {
            router.navigateTo(component.postsFeatureApi.getScreen(effect.userId))
        }
    }

    private fun TextInputEditText.getIntInput(): Int? =
        text?.toString()?.toIntOrNull()

    companion object {
        fun newInstance(): Fragment = LoginFragment()
    }
}
