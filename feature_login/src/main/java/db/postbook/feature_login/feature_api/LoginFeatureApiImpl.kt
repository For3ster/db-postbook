package db.postbook.feature_login.feature_api

import com.github.terrakok.cicerone.Screen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import db.postbook.domain_login.feature_api.LoginFeatureApi
import db.postbook.feature_login.LoginFragment
import javax.inject.Inject

internal class LoginFeatureApiImpl @Inject constructor() : LoginFeatureApi {

    override fun getScreen(): Screen =
        FragmentScreen { LoginFragment.newInstance() }
}
