package db.postbook.feature_posts.screens.comments.presentation

import db.postbook.domain_posts.dto.Comment
import db.postbook.domain_posts.dto.Post
import db.postbook.feature_posts.screens.comments.presentation.CommentsCommand.GetComments
import db.postbook.feature_posts.screens.comments.presentation.CommentsCommand.GetPost
import db.postbook.feature_posts.screens.comments.presentation.CommentsEffect.ShowError
import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent.Internal
import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent.Ui
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.shouldBe
import org.mockito.kotlin.mock

internal class CommentsReducerTest : BehaviorSpec({
    val reducer = CommentsReducer

    Given("Initial State") {
        When("Ui.System.Init") {
            val (state, effects, commands) = reducer.reduce(Ui.System.Init, STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") {
                commands.shouldContainExactly(
                    GetPost(POST_ID),
                    GetComments(POST_ID)
                )
            }
        }

        When("Internal.GetPostSuccess") {
            val (state, effects, commands) = reducer.reduce(Internal.GetPostSuccess(POST), STATE)
            Then("check state") {
                state.post shouldBe POST
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.GetPostError") {
            val (state, effects, commands) = reducer.reduce(Internal.GetPostError(ERROR), STATE)
            Then("check state") {
                state.post.shouldBeNull()
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldContainExactly(ShowError(ERROR)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.GetCommentsSuccess") {
            val (state, effects, commands) = reducer.reduce(
                Internal.GetCommentsSuccess(COMMENTS),
                STATE
            )
            Then("check state") {
                state.comments shouldBe COMMENTS
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.GetCommentsError") {
            val (state, effects, commands) = reducer.reduce(Internal.GetCommentsError(ERROR), STATE)
            Then("check state") {
                state.comments.shouldBeEmpty()
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldContainExactly(ShowError(ERROR)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }
}) {

    private companion object {

        const val POST_ID = 1
        val STATE = CommentsState(POST_ID)
        val ERROR = Throwable()
        val POST: Post = mock()
        val COMMENT: Comment = mock()
        val COMMENTS = List(2) { COMMENT }
    }
}
