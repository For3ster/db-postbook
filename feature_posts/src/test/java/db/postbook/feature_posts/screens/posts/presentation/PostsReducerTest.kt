package db.postbook.feature_posts.screens.posts.presentation

import db.postbook.domain_posts.dto.UserId
import db.postbook.feature_posts.screens.posts.list.PostItem
import db.postbook.feature_posts.screens.posts.presentation.PostsCommand.AddPostToFavorites
import db.postbook.feature_posts.screens.posts.presentation.PostsCommand.GetPosts
import db.postbook.feature_posts.screens.posts.presentation.PostsCommand.RemovePostFromFavorites
import db.postbook.feature_posts.screens.posts.presentation.PostsEffect.OpenComments
import db.postbook.feature_posts.screens.posts.presentation.PostsEffect.ShowError
import db.postbook.feature_posts.screens.posts.presentation.PostsEvent.Internal
import db.postbook.feature_posts.screens.posts.presentation.PostsEvent.Ui
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

internal class PostsReducerTest : BehaviorSpec({
    val reducer = PostsReducer

    Given("Initial State") {
        When("Ui.System.Init") {
            val (state, effects, commands) = reducer.reduce(Ui.System.Init, STATE)
            Then("check state") { state shouldBe STATE }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") {
                commands.shouldContainExactly(GetPosts(STATE.userId))
            }
        }

        When("Internal.GetPostsSuccess") {
            val (state, effects, commands) =
                reducer.reduce(Internal.GetPostsSuccess(POST_ITEMS), STATE)

            Then("check state") {
                state.postItems shouldBe POST_ITEMS
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Internal.GetPostsError") {
            val (state, effects, commands) = reducer.reduce(Internal.GetPostsError(ERROR), STATE)
            Then("check state") {
                state.postItems shouldBe STATE.postItems
                state.isLoading.shouldBeFalse()
            }
            Then("check effects") { effects.shouldContainExactly(ShowError(ERROR)) }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }

    Given("Loaded posts") {
        val givenState = STATE.copy(postItems = POST_ITEMS)

        When("Ui.Click.AllPosts") {
            val (state, effects, commands) = reducer.reduce(Ui.Click.AllPosts, givenState)
            Then("check state") { state.isAllPostsSelected.shouldBeTrue() }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Ui.Click.FavoritePosts") {
            val (state, effects, commands) = reducer.reduce(Ui.Click.FavoritePosts, givenState)
            Then("check state") { state.isAllPostsSelected.shouldBeFalse() }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") { commands.shouldBeEmpty() }
        }

        When("Ui.Click.PostItem") {
            val (state, effects, commands) =
                reducer.reduce(Ui.Click.PostItem(POST_ITEM.post), givenState)

            Then("check state") { state shouldBe givenState }
            Then("check effects") {
                effects.shouldContainExactly(OpenComments(POST_ITEM.post.id))
            }
            Then("check commands") { commands.shouldBeEmpty() }
        }
    }

    Given("Regular post") {
        val givenState = STATE.copy(postItems = POST_ITEMS)
        When("Ui.Click.Favorite") {
            val (state, effects, commands) =
                reducer.reduce(Ui.Click.Favorite(POST_ITEM.post), givenState)

            Then("check state") {
                state.postItems.shouldContain(POST_ITEM.run {
                    copy(isFavorite = !isFavorite)
                })
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") {
                commands.shouldContainExactly(AddPostToFavorites(POST_ITEM.post.id))
            }
        }
    }

    Given("Favorite post") {
        val givenItem: PostItem = mock {
            on { it.post } doReturn mock()
            on { it.isFavorite } doReturn true
        }
        val givenState = STATE.copy(postItems = listOf(givenItem))
        When("Ui.Click.Favorite") {
            val (state, effects, commands) =
                reducer.reduce(Ui.Click.Favorite(givenItem.post), givenState)

            Then("check state") {
                state.postItems.shouldContain(givenItem.run {
                    copy(isFavorite = !isFavorite)
                })
            }
            Then("check effects") { effects.shouldBeEmpty() }
            Then("check commands") {
                commands.shouldContainExactly(RemovePostFromFavorites(givenItem.post.id))
            }
        }
    }
}) {

    private companion object {

        val USER_ID = UserId(1)
        val STATE = PostsState(userId = USER_ID)
        val ERROR = Throwable()
        val POST_ITEM: PostItem = mock {
            on { it.post } doReturn mock()
            on { it.isFavorite } doReturn false
        }
        val POST_ITEMS = List(2) { POST_ITEM }
    }
}
