package db.postbook.feature_posts.di

import db.postbook.feature_posts.screens.comments.presentation.CommentsStoreFactory
import db.postbook.feature_posts.screens.posts.presentation.PostsStoreFactory

interface PostsDependencies {

    val postsStoreFactory: PostsStoreFactory
    val commentsStoreFactory: CommentsStoreFactory
}
