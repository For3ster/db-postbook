package db.postbook.feature_posts.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import db.postbook.domain_posts.PostsApi
import db.postbook.domain_posts.feature_api.PostsFeatureApi
import db.postbook.feature_posts.feature_api.PostsFeatureApiImpl
import db.postbook.feature_posts.screens.comments.presentation.CommentsStoreFactory
import db.postbook.feature_posts.screens.comments.presentation.CommentsStoreFactoryImpl
import db.postbook.feature_posts.screens.posts.presentation.PostsStoreFactory
import db.postbook.feature_posts.screens.posts.presentation.PostsStoreFactoryImpl
import retrofit2.Retrofit
import javax.inject.Singleton

private const val BASE_URL = "https://jsonplaceholder.typicode.com/"

@Module(includes = [PostsBindingsModule::class])
object PostsModule {

    @Provides
    @Singleton
    fun providePostsApi(
        builder: Retrofit.Builder,
    ): PostsApi =
        builder
            .baseUrl(BASE_URL)
            .build()
            .create(PostsApi::class.java)
}

@Module
abstract class PostsBindingsModule {

    @Binds
    internal abstract fun PostsFeatureApiImpl.bindPostsFeatureApi(): PostsFeatureApi

    @Binds
    internal abstract fun PostsStoreFactoryImpl.bindPostsStoreFactory(): PostsStoreFactory

    @Binds
    internal abstract fun CommentsStoreFactoryImpl.bindCommentsStoreFactory(): CommentsStoreFactory
}
