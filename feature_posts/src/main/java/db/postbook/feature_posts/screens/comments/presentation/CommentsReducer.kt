package db.postbook.feature_posts.screens.comments.presentation

import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent.Internal
import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent.Ui
import vivid.money.elmslie.core.store.dsl_reducer.ScreenDslReducer
import db.postbook.feature_posts.screens.comments.presentation.CommentsCommand as Command
import db.postbook.feature_posts.screens.comments.presentation.CommentsEffect as Effect
import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent as Event
import db.postbook.feature_posts.screens.comments.presentation.CommentsState as State

internal object CommentsReducer : ScreenDslReducer<Event, Ui, Internal,
        State, Effect, Command>(Ui::class, Internal::class) {

    override fun Result.ui(event: Ui) = when (event) {
        is Ui.System.Init -> commands {
            +Command.GetPost(state.postId)
            +Command.GetComments(state.postId)
        }
    }

    override fun Result.internal(event: Internal) = when (event) {
        is Internal.GetPostSuccess -> state { copy(post = event.post, isLoading = false) }
        is Internal.GetPostError -> {
            state { copy(isLoading = false) }
            effects { +Effect.ShowError(event.error) }
        }

        is Internal.GetCommentsSuccess -> state {
            copy(
                comments = event.comments,
                isLoading = false
            )
        }

        is Internal.GetCommentsError -> {
            state { copy(isLoading = false) }
            effects { +Effect.ShowError(event.error) }
        }
    }
}
