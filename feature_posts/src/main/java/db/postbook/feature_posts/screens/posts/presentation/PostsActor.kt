package db.postbook.feature_posts.screens.posts.presentation

import db.postbook.common_mvi.actorFlow
import db.postbook.domain_posts.PostsRepository
import db.postbook.feature_posts.screens.posts.list.PostItem
import db.postbook.feature_posts.screens.posts.presentation.PostsCommand.AddPostToFavorites
import db.postbook.feature_posts.screens.posts.presentation.PostsCommand.GetPosts
import db.postbook.feature_posts.screens.posts.presentation.PostsCommand.RemovePostFromFavorites
import db.postbook.feature_posts.screens.posts.presentation.PostsEvent.Internal
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import vivid.money.elmslie.coroutines.Actor
import javax.inject.Inject

internal class PostsActor @Inject constructor(
    private val repository: PostsRepository,
) : Actor<PostsCommand, Internal> {

    override fun execute(command: PostsCommand): Flow<Internal> =
        when (command) {
            is GetPosts -> actorFlow {
                val posts = repository.getUserPosts(command.userId)
                val favPostsIds = repository.observeFavoritePostIds().first()
                posts.map {
                    PostItem(
                        post = it,
                        isFavorite = favPostsIds.contains(it.id.toString())
                    )
                }
            }.mapEvents(Internal::GetPostsSuccess, Internal::GetPostsError)

            is AddPostToFavorites -> actorFlow {
                repository.addPostToFavorites(command.postId)
            }.mapEvents()

            is RemovePostFromFavorites -> actorFlow {
                repository.removePostFromFavorites(command.postId)
            }.mapEvents()
        }
}
