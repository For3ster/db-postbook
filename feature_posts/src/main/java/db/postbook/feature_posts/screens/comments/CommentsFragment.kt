package db.postbook.feature_posts.screens.comments

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import db.postbook.common_di.getComponent
import db.postbook.common_mvi.base.BaseFragment
import db.postbook.common_mvi.setOnBackPressedCallback
import db.postbook.common_navigation.router
import db.postbook.feature_posts.R
import db.postbook.feature_posts.databinding.FragmentCommentsBinding
import db.postbook.feature_posts.di.PostsDependencies
import db.postbook.feature_posts.screens.comments.list.CommentItemAdapter
import db.postbook.feature_posts.screens.comments.presentation.CommentsEffect
import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent.Ui
import db.postbook.feature_posts.screens.comments.presentation.CommentsState
import vivid.money.elmslie.android.storeholder.LifecycleAwareStoreHolder
import vivid.money.elmslie.android.storeholder.StoreHolder
import db.postbook.feature_posts.screens.comments.presentation.CommentsEffect as Effect
import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent as Event
import db.postbook.feature_posts.screens.comments.presentation.CommentsState as State

internal const val ARG_POST_ID = "ARG_POST_ID"

internal class CommentsFragment : BaseFragment<Event, Effect, State>(R.layout.fragment_comments) {

    private val component by lazy { getComponent<PostsDependencies>() }

    override val initEvent: Event = Ui.System.Init

    override val storeHolder: StoreHolder<Event, Effect, State> =
        LifecycleAwareStoreHolder(
            lifecycle = lifecycle,
            storeProvider = {
                component.commentsStoreFactory.create(arguments?.getInt(ARG_POST_ID)!!)
            }
        )

    private val binding by viewBinding(FragmentCommentsBinding::bind)
    private lateinit var adapter: CommentItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.apply {
            adapter = CommentItemAdapter().also { this@CommentsFragment.adapter = it }
        }

        binding.toolbar.setNavigationOnClickListener { router.exit() }
        setOnBackPressedCallback { router.exit() }
    }

    override fun render(state: CommentsState) {
        binding.progressView.root.isVisible = state.isLoading
        state.post?.let {
            binding.postTitle.text = it.title
            binding.postBody.text = it.body
        }
    }

    override fun renderList(state: CommentsState, list: List<Any>) {
        adapter.submitList(state.comments)
    }

    override fun handleEffect(effect: CommentsEffect) = when (effect) {
        is Effect.ShowError -> showError(effect.error.toString())
    }

    companion object {

        fun newInstance(postId: Int): Fragment = CommentsFragment().apply {
            arguments = bundleOf().apply { putInt(ARG_POST_ID, postId) }
        }
    }
}
