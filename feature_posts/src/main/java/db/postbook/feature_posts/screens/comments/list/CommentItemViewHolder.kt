package db.postbook.feature_posts.screens.comments.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import db.postbook.domain_posts.dto.Comment
import db.postbook.feature_posts.databinding.ItemCommentBinding

internal class CommentItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemCommentBinding.bind(itemView)

    fun bind(item: Comment) = with(binding) {
        name.text = item.name
        email.text = item.email
        body.text = item.body
    }
}
