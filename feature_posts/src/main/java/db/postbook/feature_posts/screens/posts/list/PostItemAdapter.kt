package db.postbook.feature_posts.screens.posts.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import db.postbook.feature_posts.databinding.ItemPostBinding

internal class PostItemAdapter(
    private val onItemClick: (PostItem) -> Unit,
    private val onFavoriteClick: (PostItem) -> Unit,
) : ListAdapter<PostItem, PostItemViewHolder>(ItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemPostBinding.inflate(inflater, parent, false)

        return PostItemViewHolder(binding.root, onItemClick, onFavoriteClick)
    }

    override fun onBindViewHolder(holder: PostItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

private class ItemDiffCallback : DiffUtil.ItemCallback<PostItem>() {

    override fun areItemsTheSame(oldItem: PostItem, newItem: PostItem): Boolean =
        oldItem.post.id == newItem.post.id

    override fun areContentsTheSame(oldItem: PostItem, newItem: PostItem): Boolean =
        oldItem.isFavorite == newItem.isFavorite
}
