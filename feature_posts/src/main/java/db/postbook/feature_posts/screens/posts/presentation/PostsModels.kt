package db.postbook.feature_posts.screens.posts.presentation

import db.postbook.domain_posts.dto.Post
import db.postbook.domain_posts.dto.UserId
import db.postbook.feature_posts.screens.posts.list.PostItem
import vivid.money.elmslie.core.store.Store

typealias PostsStore = Store<PostsEvent, PostsEffect, PostsState>

data class PostsState(
    val userId: UserId,
    val postItems: List<PostItem> = emptyList(),
    val isLoading: Boolean = true,
    val isAllPostsSelected: Boolean = true,
)

sealed interface PostsEvent {

    sealed interface Ui : PostsEvent {
        object System {
            object Init : Ui
        }

        object Click {
            data class PostItem(val post: Post) : Ui
            data class Favorite(val post: Post) : Ui

            object AllPosts : Ui
            object FavoritePosts : Ui
        }
    }

    sealed interface Internal : PostsEvent {
        data class GetPostsSuccess(val posts: List<PostItem>) : Internal
        data class GetPostsError(val error: Throwable) : Internal
    }
}

sealed interface PostsEffect {
    data class ShowError(val error: Throwable) : PostsEffect
    data class OpenComments(val postId: Int) : PostsEffect
}

sealed interface PostsCommand {
    data class GetPosts(val userId: UserId) : PostsCommand

    data class AddPostToFavorites(val postId: Int) : PostsCommand
    data class RemovePostFromFavorites(val postId: Int) : PostsCommand
}
