package db.postbook.feature_posts.screens.posts.list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import db.postbook.feature_posts.R
import db.postbook.feature_posts.databinding.ItemPostBinding

internal class PostItemViewHolder(
    itemView: View,
    private val onItemClick: (PostItem) -> Unit,
    private val onFavoriteClick: (PostItem) -> Unit,
) : RecyclerView.ViewHolder(itemView) {

    private val binding = ItemPostBinding.bind(itemView)

    fun bind(item: PostItem) = with(binding) {
        title.text = item.post.title
        body.text = item.post.body

        val favRes = if (item.isFavorite) R.drawable.ic_favorite else R.drawable.ic_favorite_border
        favoriteIcon.setImageResource(favRes)

        root.setOnClickListener { onItemClick(item) }
        favoriteIcon.setOnClickListener { onFavoriteClick(item) }
    }
}
