package db.postbook.feature_posts.screens.posts.presentation

import db.postbook.feature_posts.screens.posts.presentation.PostsEvent.Internal
import db.postbook.feature_posts.screens.posts.presentation.PostsEvent.Ui
import vivid.money.elmslie.core.store.dsl_reducer.ScreenDslReducer
import db.postbook.feature_posts.screens.posts.presentation.PostsCommand as Command
import db.postbook.feature_posts.screens.posts.presentation.PostsEffect as Effect
import db.postbook.feature_posts.screens.posts.presentation.PostsEvent as Event
import db.postbook.feature_posts.screens.posts.presentation.PostsState as State

internal object PostsReducer : ScreenDslReducer<Event, Ui, Internal,
        State, Effect, Command>(Ui::class, Internal::class) {

    override fun Result.ui(event: Ui) = when (event) {
        is Ui.System.Init -> commands { +Command.GetPosts(state.userId) }
        is Ui.Click.AllPosts -> state { copy(isAllPostsSelected = true) }
        is Ui.Click.FavoritePosts -> state { copy(isAllPostsSelected = false) }
        is Ui.Click.Favorite -> {
            val changedPost = state.postItems.first { it.post.id == event.post.id }
            state {
                val updatedItems = postItems - changedPost + changedPost.run {
                    copy(isFavorite = !isFavorite)
                }
                copy(postItems = updatedItems)
            }

            commands {
                +if (changedPost.isFavorite) {
                    Command.RemovePostFromFavorites(changedPost.post.id)
                } else {
                    Command.AddPostToFavorites(changedPost.post.id)
                }
            }
        }

        is Ui.Click.PostItem -> effects { +Effect.OpenComments(event.post.id) }
    }

    override fun Result.internal(event: Internal) = when (event) {
        is Internal.GetPostsSuccess -> state {
            copy(postItems = event.posts, isLoading = false)
        }

        is Internal.GetPostsError -> {
            state { copy(isLoading = false) }
            effects { +Effect.ShowError(event.error) }
        }
    }
}
