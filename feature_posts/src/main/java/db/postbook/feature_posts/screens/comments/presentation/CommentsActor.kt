package db.postbook.feature_posts.screens.comments.presentation

import db.postbook.common_mvi.actorFlow
import db.postbook.domain_posts.PostsApi
import db.postbook.feature_posts.screens.comments.presentation.CommentsCommand.GetComments
import db.postbook.feature_posts.screens.comments.presentation.CommentsCommand.GetPost
import db.postbook.feature_posts.screens.comments.presentation.CommentsEvent.Internal
import kotlinx.coroutines.flow.Flow
import vivid.money.elmslie.coroutines.Actor
import javax.inject.Inject

internal class CommentsActor @Inject constructor(
    private val postsApi: PostsApi,
) : Actor<CommentsCommand, Internal> {

    override fun execute(command: CommentsCommand): Flow<Internal> =
        when (command) {
            is GetPost -> actorFlow {
                postsApi.getPost(command.postId)
            }.mapEvents(Internal::GetPostSuccess, Internal::GetPostError)

            is GetComments -> actorFlow {
                postsApi.getComments(command.postId)
            }.mapEvents(Internal::GetCommentsSuccess, Internal::GetCommentsError)
        }
}
