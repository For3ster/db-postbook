package db.postbook.feature_posts.screens.posts.presentation

import db.postbook.domain_posts.dto.UserId
import vivid.money.elmslie.coroutines.ElmStoreCompat
import javax.inject.Inject

interface PostsStoreFactory {

    fun create(userId: UserId): PostsStore
}

internal class PostsStoreFactoryImpl @Inject constructor(
    private val actor: PostsActor,
) : PostsStoreFactory {

    override fun create(userId: UserId): PostsStore = ElmStoreCompat(
        initialState = PostsState(userId),
        reducer = PostsReducer,
        actor = actor
    )
}
