package db.postbook.feature_posts.screens.comments.presentation

import vivid.money.elmslie.coroutines.ElmStoreCompat
import javax.inject.Inject

interface CommentsStoreFactory {

    fun create(postId: Int): CommentsStore
}

internal class CommentsStoreFactoryImpl @Inject constructor(
    private val actor: CommentsActor,
) : CommentsStoreFactory {

    override fun create(postId: Int): CommentsStore = ElmStoreCompat(
        initialState = CommentsState(postId),
        reducer = CommentsReducer,
        actor = actor
    )
}
