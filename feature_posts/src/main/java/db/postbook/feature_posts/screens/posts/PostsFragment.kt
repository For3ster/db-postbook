package db.postbook.feature_posts.screens.posts

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import by.kirich1409.viewbindingdelegate.viewBinding
import com.github.terrakok.cicerone.androidx.FragmentScreen
import db.postbook.common_di.getComponent
import db.postbook.common_mvi.base.BaseFragment
import db.postbook.common_mvi.setOnBackPressedCallback
import db.postbook.common_navigation.router
import db.postbook.domain_posts.dto.UserId
import db.postbook.feature_posts.R
import db.postbook.feature_posts.databinding.FragmentPostsBinding
import db.postbook.feature_posts.di.PostsDependencies
import db.postbook.feature_posts.screens.comments.CommentsFragment
import db.postbook.feature_posts.screens.posts.list.PostItem
import db.postbook.feature_posts.screens.posts.list.PostItemAdapter
import db.postbook.feature_posts.screens.posts.presentation.PostsEffect
import db.postbook.feature_posts.screens.posts.presentation.PostsEvent.Ui
import db.postbook.feature_posts.screens.posts.presentation.PostsState
import vivid.money.elmslie.android.storeholder.LifecycleAwareStoreHolder
import vivid.money.elmslie.android.storeholder.StoreHolder
import db.postbook.feature_posts.screens.posts.presentation.PostsEffect as Effect
import db.postbook.feature_posts.screens.posts.presentation.PostsEvent as Event
import db.postbook.feature_posts.screens.posts.presentation.PostsState as State

internal const val ARG_USER_ID = "ARG_USER_ID"

internal class PostsFragment : BaseFragment<Event, Effect, State>(R.layout.fragment_posts) {

    private val userId: UserId by lazy { UserId(arguments?.getInt(ARG_USER_ID)!!) }
    private val component by lazy { getComponent<PostsDependencies>() }

    override val initEvent: Event = Ui.System.Init

    override val storeHolder: StoreHolder<Event, Effect, State> =
        LifecycleAwareStoreHolder(
            lifecycle = lifecycle,
            storeProvider = { component.postsStoreFactory.create(userId) }
        )

    private val binding by viewBinding(FragmentPostsBinding::bind)
    private lateinit var adapter: PostItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.bottomNavigation.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.all_posts -> {
                    store.accept(Ui.Click.AllPosts)
                    true
                }

                R.id.fav_posts -> {
                    store.accept(Ui.Click.FavoritePosts)
                    true
                }

                else -> false
            }
        }

        binding.recyclerView.apply {
            adapter = createAdapter().also { this@PostsFragment.adapter = it }
        }

        binding.toolbar.setNavigationOnClickListener { router.exit() }
        setOnBackPressedCallback { router.exit() }
    }

    override fun render(state: PostsState) {
        binding.progressView.root.isVisible = state.isLoading
    }

    override fun mapList(state: PostsState): List<PostItem> =
        if (state.isAllPostsSelected) state.postItems
        else state.postItems.filter { it.isFavorite }

    @Suppress("UNCHECKED_CAST")
    override fun renderList(state: PostsState, list: List<Any>) {
        adapter.submitList((list as List<PostItem>).sortedBy { it.post.id })
    }

    override fun handleEffect(effect: PostsEffect) = when (effect) {
        is Effect.OpenComments -> {
            val screen = FragmentScreen { CommentsFragment.newInstance(effect.postId) }
            router.navigateTo(screen)
        }

        is Effect.ShowError -> showError(effect.error.toString())
    }

    private fun createAdapter() =
        PostItemAdapter(
            onItemClick = { store.accept(Ui.Click.PostItem(it.post)) },
            onFavoriteClick = { store.accept(Ui.Click.Favorite(it.post)) }
        )

    companion object {

        fun newInstance(userId: UserId): Fragment = PostsFragment().apply {
            arguments = bundleOf().apply { putInt(ARG_USER_ID, userId.id) }
        }
    }
}
