package db.postbook.feature_posts.screens.posts.list

import db.postbook.domain_posts.dto.Post

data class PostItem(
    val post: Post,
    val isFavorite: Boolean,
)
