package db.postbook.feature_posts.screens.comments.presentation

import db.postbook.domain_posts.dto.Comment
import db.postbook.domain_posts.dto.Post
import vivid.money.elmslie.core.store.Store

typealias CommentsStore = Store<CommentsEvent, CommentsEffect, CommentsState>

data class CommentsState(
    val postId: Int,
    val post: Post? = null,
    val comments: List<Comment> = emptyList(),
    val isLoading: Boolean = true,
)

sealed interface CommentsEvent {

    sealed interface Ui : CommentsEvent {
        object System {
            object Init : Ui
        }
    }

    sealed interface Internal : CommentsEvent {
        data class GetPostSuccess(val post: Post) : Internal
        data class GetPostError(val error: Throwable) : Internal

        data class GetCommentsSuccess(val comments: List<Comment>) : Internal
        data class GetCommentsError(val error: Throwable) : Internal
    }
}

sealed interface CommentsEffect {
    data class ShowError(val error: Throwable) : CommentsEffect
}

sealed interface CommentsCommand {
    data class GetPost(val postId: Int) : CommentsCommand
    data class GetComments(val postId: Int) : CommentsCommand
}
