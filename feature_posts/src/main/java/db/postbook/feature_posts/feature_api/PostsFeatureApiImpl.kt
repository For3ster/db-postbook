package db.postbook.feature_posts.feature_api

import com.github.terrakok.cicerone.Screen
import com.github.terrakok.cicerone.androidx.FragmentScreen
import db.postbook.domain_posts.dto.UserId
import db.postbook.domain_posts.feature_api.PostsFeatureApi
import db.postbook.feature_posts.screens.posts.PostsFragment
import javax.inject.Inject

internal class PostsFeatureApiImpl @Inject constructor() : PostsFeatureApi {

    override fun getScreen(userId: UserId): Screen =
        FragmentScreen { PostsFragment.newInstance(userId) }
}
