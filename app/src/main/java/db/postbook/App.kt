package db.postbook

import android.app.Application
import com.github.terrakok.cicerone.Router
import db.postbook.common_di.ComponentHolder
import db.postbook.common_navigation.di.RouterHolder
import db.postbook.di.AppComponent
import db.postbook.di.DaggerAppComponent

class App : Application(),
    ComponentHolder<Any>,
    RouterHolder {

    lateinit var appComponent: AppComponent

    override val router: Router
        get() = appComponent.router

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.factory().create(this, this)
    }

    override fun getComponent(componentClass: Class<out Any>): Any =
        if (componentClass.isInstance(appComponent)) {
            appComponent
        } else {
            error("Component $componentClass is not declared")
        }
}
