package db.postbook

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.terrakok.cicerone.androidx.AppNavigator
import db.postbook.common_di.getComponent
import db.postbook.di.MainDependencies

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val component by lazy { getComponent<MainDependencies>() }
    private val navHolder by lazy { component.navHolder }
    private val navigator by lazy { AppNavigator(this, R.id.container) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component.router.newRootScreen(component.loginFeatureApi.getScreen())
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navHolder.removeNavigator()
        super.onPause()
    }
}
