package db.postbook.di

import android.app.Application
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import db.postbook.common_datastore.di.DataStoreModule
import db.postbook.common_navigation.di.NavigationModule
import db.postbook.common_network.di.NetworkModule
import db.postbook.feature_login.di.LoginDependencies
import db.postbook.feature_login.di.LoginModule
import db.postbook.feature_posts.di.PostsDependencies
import db.postbook.feature_posts.di.PostsModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NavigationModule::class,
        NetworkModule::class,
        DataStoreModule::class,
        LoginModule::class,
        PostsModule::class,
    ]
)
interface AppComponent :
    MainDependencies,
    LoginDependencies,
    PostsDependencies {

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context,
            @BindsInstance application: Application
        ): AppComponent
    }
}
