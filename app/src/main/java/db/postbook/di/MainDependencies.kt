package db.postbook.di

import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import db.postbook.domain_login.feature_api.LoginFeatureApi

interface MainDependencies {

    val router: Router
    val navHolder: NavigatorHolder
    val loginFeatureApi: LoginFeatureApi
}
